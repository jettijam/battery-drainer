package project.team.batterydrainer.Service;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import java.math.BigInteger;

import project.team.batterydrainer.Util.DrainerWakeLock;
import project.team.batterydrainer.Util.HardWork;

public class BatteryPowerDrainService extends JobService {

    DrainTask mDT;
    static final String TAG = "PowerDrainService";
    public BatteryPowerDrainService() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        DrainerWakeLock.acquireCpuWakeLock(getApplicationContext());
        mDT= new DrainTask();
        mDT.execute();
        return START_STICKY;

    }

    @Override
    public boolean onStartJob(JobParameters params) {
        DrainerWakeLock.acquireCpuWakeLock(getApplicationContext());
        mDT = new DrainTask();
        mDT.execute();
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        if(mDT != null)
            mDT.cancel(true);
        DrainerWakeLock.releaseCpuLock();
        return false;
    }


    class DrainTask extends AsyncTask<Void, Void, Void>{

        public DrainTask(){
            Log.i(TAG, "Power Drain Task created");
        }

        @Override
        protected Void doInBackground(Void... params) {

            while(true)
                HardWork.fibBig(BigInteger.valueOf(10000));

        }
    }
}
