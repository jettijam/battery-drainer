package project.team.batterydrainer.Service;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import java.security.NoSuchAlgorithmException;

import project.team.batterydrainer.Util.DrainerWakeLock;
import project.team.batterydrainer.Util.HardWork;

public class BatteryDrainService extends JobService {
    private static final String TAG = "BatteryDrainService";
    DrainTask mDT;
    public BatteryDrainService() {
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        DrainerWakeLock.acquireCpuWakeLock(getApplicationContext());
        try {
            mDT= new DrainTask();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return -1;
        }
        mDT.execute();
        return START_STICKY;

    }

    @Override
    public boolean onStartJob(JobParameters params) {

        DrainerWakeLock.acquireCpuWakeLock(getApplicationContext());

        try {
            mDT = new DrainTask();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        mDT.execute();
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        if(mDT!=null)
            mDT.cancel(true);
        DrainerWakeLock.releaseCpuLock();
        return false;
    }

    class DrainTask extends AsyncTask<Void, Void, Boolean>{
        HardWork mHardWork;

        public DrainTask() throws NoSuchAlgorithmException {
            mHardWork = new HardWork();
            Log.i(TAG, "Created DrainTask");
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            byte[] result = mHardWork.hash("START".getBytes());
            while(result!=null) {
                result = mHardWork.hash(result);
//                Log.i(TAG, "WORKING");
            }
            return null;
        }
    }
}
