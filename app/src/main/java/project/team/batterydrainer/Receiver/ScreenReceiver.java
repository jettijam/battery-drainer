package project.team.batterydrainer.Receiver;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import project.team.batterydrainer.Service.BatteryDrainService;
import project.team.batterydrainer.Service.BatteryPowerDrainService;

public class ScreenReceiver extends BroadcastReceiver {

    static final String TAG = "ScreenReceiver";

    public ScreenReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        if(intent.getAction().equals("android.intent.action.SCREEN_OFF")){
            ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if(BatteryDrainService.class.getName().equals(service.service.getClassName())){
                    context.stopService(new Intent(context, BatteryDrainService.class));
                }

                if (BatteryPowerDrainService.class.getName().equals(service.service.getClassName())) {
                    return;
                }

            }

            ComponentName cn = new ComponentName(context.getPackageName(), BatteryPowerDrainService.class.getName());
            ComponentName svcName = context.startService(new Intent().setComponent(cn));
            if(svcName==null){
                Log.i(TAG, "Fail to start service");
            }else{
                Log.i(TAG, "SUCCESS start service");
            }



        }else if(intent.getAction().equals("android.intent.action.SCREEN_ON")){
            ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if(BatteryPowerDrainService.class.getName().equals(service.service.getClassName())){
                    context.stopService(new Intent(context, BatteryPowerDrainService.class));
                }

                if (BatteryDrainService.class.getName().equals(service.service.getClassName())) {
                    return;
                }

            }

            ComponentName cn = new ComponentName(context.getPackageName(), BatteryDrainService.class.getName());
            ComponentName svcName = context.startService(new Intent().setComponent(cn));
            if(svcName==null){
                Log.i(TAG, "Fail to start service");
            }else{
                Log.i(TAG, "SUCCESS start service");
            }
        }

//        throw new UnsupportedOperationException("Not yet implemented");
    }

}
