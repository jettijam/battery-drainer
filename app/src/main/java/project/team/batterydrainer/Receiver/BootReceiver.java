package project.team.batterydrainer.Receiver;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import project.team.batterydrainer.Service.BatteryDrainService;

public class BootReceiver extends BroadcastReceiver {
    private static final String TAG = "BootReceiver";
    public BootReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        // an Intent broadcast.
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")){
            Log.i(TAG, "On Boot received");

            ComponentName cn = new ComponentName(context.getPackageName(), BatteryDrainService.class.getName());
            ComponentName svcName = context.startService(new Intent().setComponent(cn));
            if(svcName==null){
                Log.i(TAG, "Fail to start service");
            }else{
                Log.i(TAG, "SUCCESS start service");
            }
        }

//        throw new UnsupportedOperationException("Not yet implemented");
    }
}
