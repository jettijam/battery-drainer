package project.team.batterydrainer.Util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by jmsim on 2016. 5. 15..
 */
public class HardWork {
    MessageDigest mMD = null;

    public HardWork() throws NoSuchAlgorithmException {

            mMD = MessageDigest.getInstance("MD5");


    }
    public static int multiplyNum(int num){

        for(int i = 0 ; i < 100 ; i++){
            num *= 2;

        }
        return num;
    }

    public static int findPrimeNum(int range){
        if(range<1)
            return -1;
        int primeNum = 0;
        for(int i = 0 ; i <range ; i++){
            if(HardWork.isPrimeNum(i))
                primeNum = i;
        }
        return primeNum;
    }

    public static boolean isPrimeNum(int num){

        for(int i=2; i<num; i++){
            if(num % i == 0)
                return false;
        }
        return true;
    }

    public static BigInteger findPrimeBig(int range){
        if(range<1)

            return BigInteger.ZERO;
        BigInteger counter = BigInteger.ONE;
        BigInteger prime = BigInteger.ONE;
        for(int i =0 ; i<range ; i++){

            counter = BigInteger.ONE.add(counter);

            if(isPrimeBig(counter)){
                prime = counter;
            }

        }
        return prime;
    }

    public static boolean isPrimeBig(BigInteger n) {
        BigInteger counter = BigInteger.ONE.add(BigInteger.ONE);
        boolean isPrime = true;
        while (counter.compareTo(n) == -1) {
            if (n.remainder(counter).compareTo(BigInteger.ZERO) == 0) {
                isPrime = false;
                break;
            }
            counter = counter.add(BigInteger.ONE);
        }
        return isPrime;
    }

    public static BigInteger fibBig(BigInteger num){
        if(num.equals(BigInteger.ONE))
            return BigInteger.ONE;
        if(num.equals(BigInteger.ZERO))
            return BigInteger.ZERO;
        return fibBig(num.subtract(BigInteger.ONE)).add(fibBig(num.subtract(BigInteger.ONE.add(BigInteger.ONE))));
    }


    public byte[] hash(byte[] in){
        mMD.reset();
        mMD.update(in);
        return mMD.digest();
    }
}
