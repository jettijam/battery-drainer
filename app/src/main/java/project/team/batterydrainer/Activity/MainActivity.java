package project.team.batterydrainer.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.TextView;

import project.team.batterydrainer.R;
import project.team.batterydrainer.Receiver.BootReceiver;
import project.team.batterydrainer.Service.BatteryDrainService;

public class MainActivity extends AppCompatActivity {

    private TextView mBatteryInfo;
    private BootReceiver mBootReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBootReceiver = new BootReceiver();

        mBatteryInfo = (TextView)findViewById(R.id.main_battery_text);



//        JobScheduler jobScheduler = (JobScheduler) getApplicationContext().getSystemService(JOB_SCHEDULER_SERVICE);
//        ComponentName drainComponentName = new ComponentName(getApplicationContext(), BatteryDrainService.class);
//        JobInfo jobInfo = new JobInfo.Builder(1, drainComponentName).setRequiresCharging(false).setPersisted(true).setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY).build();
//        jobScheduler.schedule(jobInfo);
//
//        ComponentName powerDrainComponentName = new ComponentName(getApplicationContext(), BatteryPowerDrainService.class);
//        jobInfo = new JobInfo.Builder(2, powerDrainComponentName).setRequiresCharging(false).setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY).setRequiresDeviceIdle(true).build();
//        jobScheduler.schedule(jobInfo);
        //register a receiver with an intent filter.
        this.registerReceiver(this.batteryInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
//        this.registerReceiver(this.mBootReceiver, new IntentFilter(Intent.ACTION_BOOT_COMPLETED));
        Intent serviceIntent = new Intent(this, BatteryDrainService.class);
        startService(serviceIntent);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        unregisterReceiver(this.batteryInfoReceiver);
//        unregisterReceiver(this.mBootReceiver);
    }


    // IntentFilter is listening for the ACTION_BATTERY_CHANGED
    private BroadcastReceiver batteryInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            int level =intent.getIntExtra(BatteryManager.EXTRA_LEVEL,0);
            int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE,0);
            int temperature = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE,0);
            int voltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE,0);

            mBatteryInfo.setText("Level : "+level+"\n\n"+
                    "Scale : "+scale+"\n\n"+
                    "Temperature : "+temperature/10+"ºC\n\n"+
                    "Voltage : "+voltage+"mV\n\n");

            ProgressBar progressBar = (ProgressBar)findViewById(R.id.progressbar);
            progressBar.setProgress(level);

            TextView progressText = (TextView)findViewById(R.id.progressText);
            progressText.setText(level+"%");
        }

    };
}
